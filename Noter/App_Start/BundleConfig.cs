﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace Noter
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/model").Include(
                "~/Scripts/myapp/*ViewModel.js",
                "~/Scripts/myapp/AppModel.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                "~/Scripts/knockout-{version}.js",
                "~/Scripts/knockout.validation.js",
                "~/Scripts/knockout.mapping-latest.debug.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/sammy-{version}.js",
                "~/Scripts/app/common.js",
                "~/Scripts/app/app.datamodel.js",
                "~/Scripts/app/app.viewmodel.js",
                "~/Scripts/app/home.viewmodel.js",
                "~/Scripts/app/_run.js"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/jasny-bootstrap.min.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.css",
                "~/Content/jasny-bootstrap.min.css",
                 "~/Content/Site.css"));

            bundles.Add(new StyleBundle("~/css/fa").Include(
                "~/Content/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/css/bcp").Include(
                "~/Content/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"));
                
            bundles.Add(new ScriptBundle("~/js/bcp").Include(
                "~/Scripts/bootstrap-colorpicker.min.js"));

            bundles.Add(new StyleBundle("~/css/summernote").Include(
                "~/Scripts/summernote/summernote.css"));

            bundles.Add(new ScriptBundle("~/js/summernote").Include(
                "~/Scripts/summernote/summernote.min.js"));
        }
    }
}
