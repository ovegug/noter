﻿function Tag(id, name, color) {
    this.id = id;
    this.name = name;
    this.color = color;
}

function TagViewModel() {
    var self = this;

    self.tags = ko.observableArray([]);

    self.added = ko.observable(new Tag(0, '', ''));

    self.init = function () {
        $.getJSON("/api/Tags/").done(function (data) {
            ko.mapping.fromJS(data, {}, self.tags);
        });
    }

    self.showAddForm = function () {
        $('#myTagColorPicker').colorpicker();
        $('#myTagAdd').modal();
    }

    self.add = function () {
        self.added().color = $('#myTagColor').val();
        $.post(
            '/api/Tags/',
            self.added(),
            function (data) {
                self.tags.push(ko.mapping.fromJS(data));
            },
            'json');
        $('#myTagAdd').modal('hide');

        self.added(new Tag(0, '', ''));
    };


    self.remove = function (tag) {
        $.ajax({
            url: '/api/Tags/' + tag.id(),
            type: 'DELETE'
        });
        self.tags.remove(tag);
    };
}
