﻿function Note(id, title, content) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.datetime = '';
}

function NoteViewModel() {
    var self = this;


    self.notes = ko.observableArray([]);


    self.init = function () {
        $.getJSON("/api/Notes/").done(function (data) {
            ko.mapping.fromJS(data, {}, self.notes);
        });
    }

    // начало добавление тега

    self.choosenTag = ko.observable(new Tag(0, '', ''));

    self.showAddTag = function (note) {
        self.choosen(note);
        $("#noteTagAdd").modal();
    }

    self.addTag = function () {
        $.post(
            '/api/Notes/' + self.choosen().id() + '/Tag/' + self.choosenTag(),
            self.choosenTag(),
            function (data) {
                self.init();
                //self.notes.remove(self.choosen());
                //self.notes.push(ko.mapping.fromJS(data));
            },
            'json');

        self.choosenTag('');

        $("#noteTagAdd").modal('hide');
    }

    // конец добавления тега


    // начало фильтрации

    self.filter = ko.observable('*');

    // TODO: тут косяк какой-то, надо бы разобраться
    self.filterNotes = ko.computed(function () {
        if (!self.filter())
            return self.notes();
        else
            return ko.utils.arrayFilter(self.notes(),
                function (note) {
                    var result = false;
                    ko.utils.arrayForEach(note.tags(), function (tag) {
                        if (tag.id() == self.filter())
                            result = true;
                    });
                    return result;
                });
    });

    // конец фильтрации


    // начало отображения элемента

    self.choosen = ko.observable(new Note(0, 'qweqwe', 'asdas'));

    self.show = function (note) {
        self.choosen(note);
        $("#noteShow").modal();
    };

    // конец отображения элемента


    // начало добавления

    self.added = ko.observable(new Note(0, '', ''));

    self.showAddForm = function () {
        $('#noteContent').summernote({ height: 300 });
        $('#noteAdd').modal();
    }

    self.add = function () {
        self.added().content = $('#noteContent').code();
        $('#noteAdd').modal('hide');

        $.post(
            '/api/Notes/',
            self.added(),
            function (data) {
                self.notes.push(ko.mapping.fromJS(data));
            },
            'json');

        self.added(new Note(0, '', ''));
    };

    // конец добавления


    // начало редактирования

    self.edited = ko.observable(new Note(0, '', ''));

    self.showEdit = function (note) {
        self.edited(note);
        $('#noteContentEdit').summernote({ height: 300 });
        $('#noteEdit').modal('show');
    }

    self.edit = function () {
        self.edited().content = $('#noteContentEdit').code();

        $.ajax({
            url: '/api/Notes/' + self.edited().id(),
            type: 'PUT',
            data: self.edited()
        });

        $('#noteEdit').modal('hide');
    }

    // конец редактирвоания


    self.remove = function (note) {
        $.ajax({
            url: '/api/Notes/' + note.id(),
            type: 'DELETE'
        });
        self.notes.remove(note);
    };


    self.notifications = ko.computed(function () {
        return ko.utils.arrayFilter(self.notes(), function (note) {
            if (note.created())
                return true;
            return false;
        })
    });

    self.showAddNotification = function (note) {
        self.choosen(note);
        $('#myDateTime').datetimepicker();
        $("#noteNotificationAdd").modal();
    }

    self.addNotification = function () {
        self.choosen().datetime = $('myDateTime').val();
        $.ajax({
            url: 'api/Notes/' + self.choosen().id(),
            type: 'PUT',
            data: self.choosen()
        });
        $("#noteNotificationAdd").modal('hide');
    }
}
