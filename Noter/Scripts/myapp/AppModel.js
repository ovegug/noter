﻿function ApplicationModel() {
    var self = this;

    self.noteViewModel = new NoteViewModel();
    self.noteViewModel.init();

    self.tagViewModel = new TagViewModel();
    self.tagViewModel.init();

    self.fileViewModel = new FileViewModel();
    self.fileViewModel.init();
}

ApplicationModel.getToken = function () {
    return '';
}

ko.applyBindings(new ApplicationModel());