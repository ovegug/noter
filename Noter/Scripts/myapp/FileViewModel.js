﻿function FileWrapper(id, name, type, path) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.type = type;
}

function FileViewModel() {
    var self = this;

    self.files = ko.observableArray([]);

    self.init = function () {
        $.getJSON("/api/Files/").done(function (data) {
            ko.mapping.fromJS(data, {}, self.files);
        });
    }


    self.upload = function () {
        var data = new FormData();
        
        var file = $("#fileUpload").get(0).files[0];

        data.append("file", file);
        
        $.ajax({
            type: 'POST',
            url: '/api/Files/',
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                self.files.push(ko.mapping.fromJS(data));
            }
            });
    }

    self.remove = function (file) {
        $.ajax({
            url: '/api/Files/' + file.id(),
            type: 'DELETE'
        });
        self.files.remove(file);
    }
}