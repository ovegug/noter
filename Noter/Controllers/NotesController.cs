﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Noter.Models;

namespace Noter.Controllers
{
    public class NotesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Notes
        public IQueryable<NoteViewModel> GetNotes()
        {
            var notes = db.Notes.ToArray().Select(
                new Func<Note, NoteViewModel>(
                    note => NoteViewModel.CreateFromNote(note)
                ));

            return notes.AsQueryable();
        }

        // GET: api/Notes/5
        [ResponseType(typeof(NoteViewModel))]
        public IHttpActionResult GetNote(int id)
        {
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            return Ok(NoteViewModel.CreateFromNote(note));
        }

        // PUT: api/Notes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNote(int id, Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != note.Id)
            {
                return BadRequest();
            }

            db.Entry(note).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(NoteViewModel))]
        [HttpPost]
        [Route("api/Notes/{id}/Tag/{tagId}")]
        public IHttpActionResult PostTag(int id, int tagId)
        {
            var nt = new NoteTag() { NoteId = id, TagId = tagId };
            db.NoteTags.Add(nt);
            db.SaveChanges();
            return Ok(NoteViewModel.CreateFromNote(db.Notes.Find(id)));
        }

        // POST: api/Notes
        [ResponseType(typeof(Note))]
        [HttpPost]
        public IHttpActionResult PostNote(Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //заменить на привязку к авторизованному пользователю
            note.ApplicationUserId = "0f71fb85-c6e9-48ac-aa9a-882bf9c82f66";

            db.Notes.Add(note);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = note.Id }, note);
        }


        // DELETE: api/Notes/5
        [ResponseType(typeof(Note))]
        public IHttpActionResult DeleteNote(int id)
        {
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            db.Notes.Remove(note);
            db.SaveChanges();

            return Ok(note);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoteExists(int id)
        {
            return db.Notes.Count(e => e.Id == id) > 0;
        }
    }
}