﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Noter.Models;
using System.Web;
using System.IO;
using Microsoft.AspNet.Identity;

namespace Noter.Controllers
{
    public class FilesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Files
        public IQueryable<FileWrapper> GetFiles()
        {
            return db.Files;
        }

        // GET: api/Files/5
        [ResponseType(typeof(FileWrapper))]
        public async Task<IHttpActionResult> GetFile(int id)
        {
            FileWrapper file = await db.Files.FindAsync(id);
            if (file == null)
            {
                return NotFound();
            }

            return Ok(file);
        }

        // PUT: api/Files/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFile(int id, FileWrapper file)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != file.Id)
            {
                return BadRequest();
            }

            db.Entry(file).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Files
        [ResponseType(typeof(FileWrapper))]
        public async Task<IHttpActionResult> PostFile()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var file = HttpContext.Current.Request.Files[0];


                // сохраняем файл, чтобы получить id для url
                FileWrapper fw = new FileWrapper()
                {
                    Name = file.FileName,
                    Type = file.ContentType,
                    ApplicationUserId = GetCurrentUserId()
                };
                db.Files.Add(fw);
                await db.SaveChangesAsync();


                // обновляем url к файлу
                fw.Location = GetFileUrl(fw.Id);
                db.Entry(fw).State = EntityState.Modified;
                await db.SaveChangesAsync();


                // сохраняем сам файл
                var fileSavePath = GetFilePath(fw.Id);

                Directory.CreateDirectory(GetDirectoryPath());

                await Task.Run(() => file.SaveAs(fileSavePath));

                return CreatedAtRoute("DefaultApi", new { id = fw.Id }, fw);
            }

            return BadRequest();
        }

        private string GetFileUrl(int id)
        {
            var fileSavePath = Path.Combine("~/files/",
                GetCurrentUserId(),
                id.ToString());

            return fileSavePath;
        }

        private string GetDirectoryPath()
        {
            var fileSavePath = Path.Combine(
                HttpContext.Current.Server.MapPath("~/files/"),
                GetCurrentUserId());

            return fileSavePath;
        }

        private string GetFilePath(int id)
        {
            var fileSavePath = Path.Combine(
                HttpContext.Current.Server.MapPath("~/files/"),
                GetCurrentUserId(),
                id.ToString());

            return fileSavePath;
        }

        // DELETE: api/Files/5
        [ResponseType(typeof(FileWrapper))]
        public async Task<IHttpActionResult> DeleteFile(int id)
        {
            FileWrapper file = await db.Files.FindAsync(id);
            if (file == null)
            {
                return NotFound();
            }

            db.Files.Remove(file);
            await db.SaveChangesAsync();

            return Ok(file);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetCurrentUserId()
        {
            return User.Identity.GetUserId() ?? ApplicationDbContext.TestUserId;
        }

        private bool FileExists(int id)
        {
            return db.Files.Count(e => e.Id == id) > 0;
        }
    }
}