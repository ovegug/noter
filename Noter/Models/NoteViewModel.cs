﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Noter.Models
{
    public class NoteViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }

        public DateTime? Created { get; set; }
        public string Color { get; set; }

        public string ApplicationUserId { get; set; }

        public IEnumerable<Tag> Tags { get; set; }

        public static NoteViewModel CreateFromNote(Note note)
        {
            var tags = note.NoteTags.Select(nt => nt.Tag);

            return new NoteViewModel()
            {
                Id = note.Id,

                Title = note.Title,
                Content = note.Content,

                Created = note.Created,
                Color = note.Color,

                ApplicationUserId = note.ApplicationUserId,

                Tags = tags
            };
        }

        public Note CreateNote()
        {
            return new Note()
            {
                Title = this.Title,
                Content = this.Content,

                Created = this.Created,
                Color = this.Color,

                ApplicationUserId = this.ApplicationUserId,
            };
        }
    }
}