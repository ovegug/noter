﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Noter.Models
{
    [Table("Files")]
    public class FileWrapper
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Location { get; set; }
        public string Type { get; set; }

        public string ApplicationUserId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}