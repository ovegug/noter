﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Noter.Models
{
    public class Note
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }

        public DateTime? Created { get; set; }
        public string Color { get; set; }

        public string ApplicationUserId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<NoteTag> NoteTags { get; set; }

        public Note()
        {
            NoteTags = new HashSet<NoteTag>();
        }
    }
}