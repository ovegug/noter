﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Noter.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }

        public string ApplicationUserId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<NoteTag> NoteTags { get; set; }

        public Tag()
        {
            NoteTags = new HashSet<NoteTag>();
        }
    }
}