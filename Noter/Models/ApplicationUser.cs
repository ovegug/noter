﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Noter.Models
{
    public partial class ApplicationUser
    {
        public override string Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public override string UserName
        {
            get
            {
                return base.UserName;
            }
            set
            {
                base.UserName = value;
            }
        }
        public override string Email
        {
            get
            {
                return base.Email;
            }
            set
            {
                base.Email = value;
            }
        }
        public override string PasswordHash
        {
            get
            {
                return base.PasswordHash;
            }
            set
            {
                base.PasswordHash = value;
            }
        }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Note> Notes { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<FileWrapper> Files { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Tag> Tags { get; set; }

        public ApplicationUser()
        {
            Notes = new HashSet<Note>();
            Files = new HashSet<FileWrapper>();
            Tags = new HashSet<Tag>();
        }
    }
}