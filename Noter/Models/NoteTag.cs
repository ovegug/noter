﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Noter.Models
{
    public class NoteTag
    {
        [Key, ForeignKey("Note"), Column(Order = 1)]
        public int NoteId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Note Note { get; set; }
        
        [Key, ForeignKey("Tag"), Column(Order = 2)]
        public int TagId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Tag Tag { get; set; }
    }
}